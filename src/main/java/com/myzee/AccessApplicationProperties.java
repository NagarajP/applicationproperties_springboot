package com.myzee;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccessApplicationProperties {

	@Value("${myzee.name}")
	private String name;
	
	@Value("${myzee.app}")
	private String welcome;
	
	@Value("${myzee.list.values}")
	private List<String> social;
	
	
	@RequestMapping("/value1")
	public String getApp1() {
		return name;
	}
	
	@RequestMapping("/value2")
	public String getApp2() {
		return welcome;
	}
	
	@RequestMapping("/value3")
	public List<String> getApp3() {
		return social;
	}
}
