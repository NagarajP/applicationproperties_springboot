package com.myzee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfiguarionPropertiesAnnotation {
	
	@Autowired
	private DbSettings dbSettings;
	
	@RequestMapping("/db")
	public String getConfigProperties() {
		return 
				"Connection : " + dbSettings.getConnection() + " \n" +
				"Username : " + dbSettings.getUsername() + " \n" +
				"Password : " + dbSettings.getPassword() + " \n" +
				"Port : " + dbSettings.getPort();
	}
}
